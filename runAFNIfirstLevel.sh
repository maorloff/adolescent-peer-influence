#!/bin/bash

for i in fMRIdata/preprocessed/*; do
	sub=$(basename $i)
	sh functions/firstLevelAFNI_OCUsafe.sh $sub
	sh functions/firstLevelAFNI_OCUrisky.sh $sub
done
rm *.err
