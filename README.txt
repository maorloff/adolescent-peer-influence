This repository contains all the analysis scripts for Chung, Orloff et al., 2020

To re-make the figures from this paper, run:
	adolDrugUsePeerInfluence.m

To see the results from the logistic regressions, run:
	logisticRegressions.R (main text analyses)
	logisticRegressionsTableS1.R (supplementary analyses)

If you would like to re-estimate model parameters, run:
	estimateModel.R

To rerun the main imaging analyses (using SPM), you will need to download the proper image
files from OpenNeuro and put them in a folder called 'fMRIdata/raw'. From there, run the
following scripts (in order):
	runFMRIpreprocess.m
	runFirstLevel.m
	runFirstLevelPPI.m
	runSecondLevel.m 
	runExtractROIs.m

To run the 3dMEMA analysis (fig S8), run:
	runFMRIpreprocess.m
	makeCensorFilesForAFNI.m
	runAFNIfirstLevel.sh
	runMMafni.sh
	extractVoxelsForMM.sh
	extractVoxelValsForMM.m

To run the Neurosynth decoding analysis (fig S9), after the main imaging analyses have
been reran, run:
	thresholdForDecoding.sh
	decodeWithNeurosynth.py
	
When you run 'adolDrugUsePeerInfluence.m', 'logisticRegressions.R', or
 'logisticRegressionsTableS1.R' after any of the above steps to re-analyze the data, the
  data from re-analysis will be used to create figures and calculate statistics.
