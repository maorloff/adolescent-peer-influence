clear; clc; close all hidden;

%% add function paths
addpath(genpath('functions'))

subsToPreproc = dir('fMRIdata/raw/sub-*');
SPM12path = '/mnt/nfs/proj/pinfadol/spm12/'; %be sure to add your own path to SPM12 here

parfor i = 1:length(subsToPreproc)
    disp(subsToPreproc(i).name)
    fMRIpreprocess(subsToPreproc(i).name(5:end),['fMRIdata/raw/' num2str(subsToPreproc(i).name)], ...
        ['fMRIdata/preprocessed/' num2str(subsToPreproc(i).name(5:end))], SPM12path)
end
