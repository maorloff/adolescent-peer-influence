#!/bin/bash

if [ -d "fMRIdata/secondLevelAFNI/" ]; then
	for i in fMRIdata/secondLevelAFNI/*; do
		3dmaskdump -o ${i}/MMvoxels.txt -noijk ${i}/MM+tlrc[5]
	done
fi

if [ -d "fMRIdata/secondLevel/" ]; then
	for i in fMRIdata/secondLevel/*SocialValueBetweenGroup; do
		3dmaskdump -o ${i}/voxels.txt -noijk ${i}/spmT_0001.nii
	done
fi
