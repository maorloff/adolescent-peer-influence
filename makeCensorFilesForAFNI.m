clear; clc; close all hidden;

addpath(genpath('functions'))

preprocessdir = 'fMRIdata/preprocessed/';
subList = dir([preprocessdir '/9*']);

for i = 1:length(subList)
    currSub = subList(i).name;
    subpreprocdir = [preprocessdir subList(i).name '/functional/'];
    
    movFile = dir([subpreprocdir 'r*.txt']);
    
    FDs = calculateFD(importdata([subpreprocdir  movFile.name]));
    saveName = [subpreprocdir 'censorFile.1D'];
    volsToInclude = FDs < 0.9;
    dlmwrite(saveName,volsToInclude,'delimiter','\n')
end

