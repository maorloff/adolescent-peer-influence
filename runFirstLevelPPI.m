clear; clc; close all hidden;

%% add function path
addpath(genpath('functions'))

DMdir = 'data/DMs/';
preprocdatadir = 'fMRIdata/preprocessed/';

SPM12path = '/mnt/nfs/proj/pinfadol/spm12/'; %be sure to add your own path to SPM12 here

subsToRun = dir([preprocdatadir '/9*']);

%% run PPIs
DMlist = dir([DMdir '*PPI*']);
for i = 1:length(DMlist)
    currDM = DMlist(i).name;
    for j = 1:length(subsToRun)
        currSub = subsToRun(j).name;
        if strcmp(currDM, 'safePPI') & strcmp(currSub, '930104')
            continue
        end
        
        outputdir = ['fMRIdata/firstLevel/' currDM '/' currSub];
        if ~exist(outputdir)
            mkdir(outputdir)
        end
        
        if contains(DMlist(i).name,'safe')
            firstleveldatadir = ['fMRIdata/firstLevel/safeValueSocial/' currSub '/'];
            PPIdir = [DMdir 'safePPI/'];
        else
            firstleveldatadir = ['fMRIdata/firstLevel/riskyValueSocial/' currSub '/'];
            PPIdir = [DMdir 'riskyPPI/'];
        end
        
        firstLevelPPI(preprocdatadir,currSub,firstleveldatadir,outputdir,SPM12path)
    end
end