params = struct();
if ismac
    base_name = '/Volumes/yourproject';
else
    base_name = '/mnt/nfs/proj/yourproject';
end
params.spmmat = fullfile(base_name, ...
    '/mnt/nfs/proj/yourproject/results/SPM.mat');
params.adjust = 0;
params.session = 1;
params.conjunction = 1;
params.contrast_index = 1; % 1 for first contrast, 2 for second etc. 
params.threshdesc = 'none';
params.thresh = 1;
params.extent = 0;
params. mask = struct('contrast', {}, 'thresh', {}, 'mtype', {});
params.radius = 6; % radius of sphere roi
