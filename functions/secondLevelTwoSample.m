function secondLevelTwoSample(firstleveldir,outputdir,contrastNum,contrastName,SPM12path)

addpath(SPM12path)
addpath(genpath('functions'))

spm('defaults', 'FMRI');
spm_jobman('initcfg');

subList = dir([firstleveldir '9*']);

load('data/allVars')

if ~exist(outputdir)
    mkdir(outputdir)
end
if exist([outputdir '/SPM.mat'])
    delete([outputdir '/SPM.mat'])
end

matlabbatch{1}.spm.stats.factorial_design.dir = {outputdir};

group1Count = 0;
group2Count = 0;
for i=1:length(subList)
    ConImg = [firstleveldir subList(i).name '/con_000' num2str(contrastNum) '.nii'];
    
    if anySubUse(i)==0
        group1Count = group1Count+1;
        matlabbatch{1}.spm.stats.factorial_design.des.t2.scans1(group1Count,:) = cellstr(ConImg);
    else
        group2Count = group2Count+1;
        matlabbatch{1}.spm.stats.factorial_design.des.t2.scans2(group2Count,:) = cellstr(ConImg);
    end
    
end

matlabbatch{1}.spm.stats.factorial_design.masking.em={['data/cerebrum.nii,1']};

matlabbatch{2}.spm.stats.fmri_est.spmmat = {[outputdir 'SPM.mat']};
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

matlabbatch{3}.spm.stats.con.spmmat = {[outputdir 'SPM.mat']};
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = contrastName;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.convec = [1 -1]; % paired t-test. t-test=[1 -1]
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';

matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = ['-' contrastName];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.convec = [-1 1]; % paired t-test. t-test=[1 -1]
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.delete = 0;
spm_jobman('run',matlabbatch)

clear matlabbatch
end

