function secondLevel(firstleveldir,outputdir,contrastNum,contrastName,SPM12path)

addpath(SPM12path)
addpath(genpath('functions'))

spm('defaults', 'FMRI');
spm_jobman('initcfg');

%quick fix to create separate contrasts for each group for neurosynth decoding
if contrastNum==3 & strcmp(contrastName,'social')
    loopNum = 3;
    load('data/allVars')
    origOutputdir = outputdir;
else
    loopNum = 1;
end

for j = 1:loopNum
    subList = dir([firstleveldir '9*']);
    
    if j == 2
        outputdir = [origOutputdir 'substNaive/'];
    elseif j == 3
        outputdir = [origOutputdir 'substExposed/'];
    end
    
    if ~exist(outputdir)
        mkdir(outputdir)
    end
    if exist([outputdir '/SPM.mat'])
        delete([outputdir '/SPM.mat'])
    end
    
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans={};
    matlabbatch{1}.spm.stats.factorial_design.dir = {outputdir};
    
    subCount = 0;
    for i=1:length(subList)
        ConImg = [firstleveldir subList(i).name '/con_000' num2str(contrastNum) '.nii'];
        if ~exist(ConImg)
            continue
        end
        if j == 1
            subCount = subCount + 1;
            matlabbatch{1}.spm.stats.factorial_design.des.t1.scans(subCount,:) = cellstr(ConImg);
        end
        if j == 2 & anySubUse(i)==0
            subCount = subCount + 1;
            matlabbatch{1}.spm.stats.factorial_design.des.t1.scans(subCount,:) = cellstr(ConImg);
        elseif j == 3 & anySubUse(i)==1
            subCount = subCount + 1;
            matlabbatch{1}.spm.stats.factorial_design.des.t1.scans(subCount,:) = cellstr(ConImg);
        end
    end
    
    matlabbatch{1}.spm.stats.factorial_design.masking.em={['data/cerebrum.nii,1']};
    
    matlabbatch{2}.spm.stats.fmri_est.spmmat = {[outputdir 'SPM.mat']};
    matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
    
    matlabbatch{3}.spm.stats.con.spmmat = {[outputdir 'SPM.mat']};
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = contrastName;
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.convec = 1; % paired t-test. t-test=[1 -1]
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    
    matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = ['-' contrastName];
    matlabbatch{3}.spm.stats.con.consess{2}.tcon.convec = -1; % paired t-test. t-test=[1 -1]
    matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{3}.spm.stats.con.delete = 0;
    spm_jobman('run',matlabbatch)
    
    clear matlabbatch
    
end
end

