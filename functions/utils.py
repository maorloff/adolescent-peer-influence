import os

import matplotlib
import neurosynth as ns
import nibabel as nb
from neurosynth.analysis import decode
from neurosynth.base.dataset import Dataset
from nilearn.image import resample_to_img
from nilearn.masking import apply_mask


def getDataset(nsdir='data/neurosynthData/'):
	try:
		dataset = Dataset.load(nsdir + 'dataset.pkl')
	except:
		dataset = Dataset(nsdir + 'database.txt')
		dataset.add_features(nsdir + 'features.txt')
		dataset.save(nsdir + 'dataset.pkl')
	return dataset


def makeImageList(imgbase, subids, contrastnum):
	imagelist = []
	for sub in subids:
		if sub == 'adolescents':
			imagelist.append(imgbase + '/spmT_000' + str(contrastnum) + '.nii')
		else:
			imagelist.append(imgbase + str(sub) + '/spmT_000' + str(contrastnum) + '.nii')
	return imagelist


def downsampleImages(imagelist):
	template = nb.loadsave.load('data/MNI152_T1_2mm_brain.nii.gz')
	newimagelist = []
	for image in imagelist:
		currimage = nb.load(image)
		newimage = resample_to_img(currimage, template)
		newname = image[: len(image) - 4] + '_2mm.nii'
		nb.save(newimage, newname)
		newimagelist.append(newname)
	return newimagelist


def decodeImage(decoder, imgbase, subids, contrastnum, saveloc, currmask = None):
	print('starting decoder for contrast ' + str(contrastnum))
	imagelist = makeImageList(imgbase, subids, contrastnum)
	imagelist = downsampleImages(imagelist)
	if currmask is not None:
		print('masking...')
		imagelist = applyMask(imagelist, currmask)
	decoder.decode(imagelist, save = saveloc)
	print('finished decoder for contrast ' + str(contrastnum))


def applyMask(imagelist, mask):
	currmask = nb.load(mask)
	newimagelist = []
	for image in imagelist:
		currimage = nb.load(image)
		newimage = applymask(currimage, currmask)
		newname = image[: len(image) - 4] + '_mask.nii'
		nb.save(newimage, newname)
		newimagelist.append(newname)
	return newimagelist
