#!/bin/bash

currSub=$1
currContrast=safe

source ./functions/@filePathsFirstLevel.sh $currSub $currContrast

imageFile=$(ls ${subDir}/swra*.nii)

3drefit -TR 2 $imageFile

3dDeconvolve -GOFORIT 9 -overwrite -bucket "${saveDir}/decon" -input "${imageFile}" -censor "${censorFile}" -polort A \
        -num_stimts 10 \
        -stim_label 1 'Group_CueP1' -stim_times 1 ${stimFile}_1.1D 'BLOCK4(0.25,1)' \
        -stim_label 2 'Group_ViewP1P2_RiskyInf' -stim_times 2 ${stimFile}_2.1D 'BLOCK4(6,1)' \
        -stim_label 3 'Group_ViewP1P2_SafeInfConf' -stim_times 3 ${stimFile}_3.1D 'BLOCK4(6,1)' \
        -stim_label 4 'Group_ViewP1P2_SafeInfNoConf' -stim_times 4 ${stimFile}_4.1D 'BLOCK4(6,1)' \
        -stim_label 5 'GroupViewP1P2_MixInfSafeConf' -stim_times 5 ${stimFile}_5.1D 'BLOCK4(6,1)' \
        -stim_label 6 'GroupViewP1P2_MixInfSafeNoConf' -stim_times 6 ${stimFile}_6.1D 'BLOCK4(6,1)' \
        -stim_label 7 'Single_ViewlotteriesSafeConf' -stim_times 7 ${stimFile}_7.1D 'BLOCK4(6,1)' \
        -stim_label 8 'Single_ViewlotteriesSafeNoConf' -stim_times 8 ${stimFile}_8.1D 'BLOCK4(6,1)' \
        -stim_label 9 'Keypress' -stim_times 9 ${stimFile}_9.1D 'BLOCK4(0.25,1)' \
        -stim_label 10 'Review' -stim_times 10 ${stimFile}_10.1D 'BLOCK4(2,1)'

3dREMLfit -overwrite -verb \
-mask data/cerebrumForAFNI.nii \
-matrix ${saveDir}/decon.xmat.1D \
-input ${imageFile} \
-gltsym 'SYM: +1*Group_ViewP1P2_SafeInfConf -0.5*Single_ViewlotteriesSafeConf -0.5*GroupViewP1P2_MixInfSafeConf +0.5*Single_ViewlotteriesSafeNoConf +0.5*GroupViewP1P2_MixInfSafeNoConf -1*Group_ViewP1P2_SafeInfNoConf' 'OCUsafe' \
-Rvar ${saveDir}/decon_REMLvar \
-Rglt ${saveDir}/OCUsafe \
-GOFORIT 9 \
-tout
