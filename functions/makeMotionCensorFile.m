function [fileName,censorMAT] = makeMotionCensorFile(FDs,outputDir,motionThreshold)
fileName = [outputDir '/multiregfile.txt'];
volsToCensor = find(FDs > motionThreshold);

censorMAT = zeros(length(FDs),length(volsToCensor));
for i = 1:length(volsToCensor)
       censorMAT(volsToCensor(i),i) = 1;
end

dlmwrite(fileName,censorMAT,'delimiter','\t')

end
