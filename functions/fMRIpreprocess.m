function fMRIpreprocess(sub,rawimagedir,outputdir,SPM12path)%,dir_struct,dir)

if isempty(SPM12path)
    error('be sure to add the SPM12 path in the ''runFMRIpreprocess'' script!')
end

voxel = [3 3 3];% voxel resample size
smoothpars = [6 6 6];% smooth parameters (FWHM)

%% ADDING PATHS + INITIALIZING
addpath(SPM12path);
addpath(genpath('functions'));

spm('defaults', 'FMRI');
spm_jobman('initcfg');

subjdirfun = [pwd '/' outputdir '/functional/'];
subjdirstr = [pwd '/' outputdir '/structural/'];

%% MAKE DIRS
if ~exist(subjdirfun)
    mkdir(subjdirfun);
else
    delete([subjdirfun '*'])
end

if ~exist(subjdirstr)
    mkdir(subjdirstr);
else
    delete([subjdirstr '*'])
end

%% ADD FILE PATHS AND UNZIP
rawfunPrefix = [pwd '/' rawimagedir '/func/'];
rawfun = [cell2mat(gunzip([rawfunPrefix 'sub-' sub '_task-pinf_bold.nii.gz']))];

rawstrPrefix = [pwd '/' rawimagedir '/anat/'];
rawstr = gunzip([rawstrPrefix 'sub-' sub '_T1w.nii.gz']);

%% SLICE TIME CORRECT
load('slice_timing.mat');

ims = expand_4d_vols(rawfun);
matlabbatch{1}.spm.temporal.st.scans{1} = ims; % modifying template
save([subjdirfun,'slice_timing.mat'], 'matlabbatch'); % save template and run the template
spm_jobman('run', matlabbatch);
clear matlabbatch;
movefile([rawfunPrefix 'a*'],subjdirfun)

%% REALIGN
load('EstRealign.mat');

ims = cellstr(expand_4d_vols([subjdirfun 'asub-' sub '_task-pinf_bold.nii']));

matlabbatch{1}.spm.spatial.realign.estwrite.data{1} = ims; % modifying template

save([subjdirfun,'est_realign.mat'], 'matlabbatch*'); % save template and run the template
spm_jobman('run', matlabbatch);
clear matlabbatch;

%% COREGISTER - estimate only
load('coreg_est_only.mat');

immean = expand_4d_vols([subjdirfun 'meanasub-' sub '_task-pinf_bold.nii']);
ims = expand_4d_vols([subjdirfun 'rasub-' sub '_task-pinf_bold.nii']);
matlabbatch{1}.spm.spatial.coreg.estimate.ref = cellstr(rawstr); % modifying template
matlabbatch{1}.spm.spatial.coreg.estimate.source = immean;
matlabbatch{1}.spm.spatial.coreg.estimate.other = ims;

save([subjdirfun,'coreg_est.mat'], 'matlabbatch'); % save template and run the template
spm_jobman('run', matlabbatch);
clear matlabbatch*

%% SEGMENT
matlabbatch{1}.spm.spatial.preproc.channel.vols = cellstr(rawstr);
matlabbatch{1}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{1}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{1}.spm.spatial.preproc.channel.write = [0 1];
matlabbatch{1}.spm.spatial.preproc.tissue(1).tpm = {[SPM12path '/tpm/TPM.nii,1']};
matlabbatch{1}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{1}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(2).tpm = {[SPM12path '/tpm/TPM.nii,2']};
matlabbatch{1}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{1}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(3).tpm = {[SPM12path '/tpm/TPM.nii,3']};
matlabbatch{1}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{1}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(4).tpm = {[SPM12path '/tpm/TPM.nii,4']};
matlabbatch{1}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{1}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(5).tpm = {[SPM12path '/tpm/TPM.nii,5']};
matlabbatch{1}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{1}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(6).tpm = {[SPM12path '/tpm/TPM.nii,6']};
matlabbatch{1}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{1}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{1}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{1}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{1}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{1}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{1}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{1}.spm.spatial.preproc.warp.write = [0 1];
matlabbatch{1}.spm.spatial.preproc.warp.vox = NaN;
matlabbatch{1}.spm.spatial.preproc.warp.bb = [NaN NaN NaN
                                              NaN NaN NaN];
                                      
save([subjdirstr,'seg.mat'], 'matlabbatch'); % save template and run the template
spm_jobman('run', matlabbatch);
clear matlabbatch;
movefile([rawstrPrefix 'c*'],subjdirstr)
movefile([rawstrPrefix 'm*'],subjdirstr)
movefile([rawstrPrefix '*.mat'],subjdirstr)
movefile([rawstrPrefix 'y*'],subjdirstr)

%% NORMALI(S/Z)E
%norm_param=spm_select('FPList',subjdirstr,'^.*seg8\.mat$');
ims = expand_4d_vols([subjdirfun 'rasub-' sub '_task-pinf_bold.nii']);
matlabbatch{1}.spm.spatial.normalise.write.subj.def = cellstr([subjdirstr 'y_sub-' sub '_T1w.nii']);
matlabbatch{1}.spm.spatial.normalise.write.subj.resample = ims;
matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = voxel;
matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 4;
matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'w';
matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -50
                                                           78 76 85];

save([subjdirfun,'norm.mat'], 'matlabbatch'); % save template and run the template
spm_jobman('run', matlabbatch);
clear matlabbatch;

%% SMOOTH
ims = expand_4d_vols([subjdirfun 'wrasub-' sub '_task-pinf_bold.nii']);

matlabbatch{1}.spm.spatial.smooth.data = cellstr(ims);
matlabbatch{1}.spm.spatial.smooth.fwhm = smoothpars;
matlabbatch{1}.spm.spatial.smooth.dtype = 0;
matlabbatch{1}.spm.spatial.smooth.im = 0;
matlabbatch{1}.spm.spatial.smooth.prefix = 's';

save([subjdirfun,'smooth.mat'], 'matlabbatch'); % save template and run the template
spm_jobman('run', matlabbatch);

    function n = spm_select_get_nbframes(file)
        % spm_select_get_nbframes(file) Get the number of volumes of a 4D nifti file, excerpt from SPM12 spm_select.m
        N   = nifti(file);
        dim = [N.dat.dim 1 1 1 1 1];
        n   = dim(4);
    end

    function out = expand_4d_vols(nifti)
        % expand_4d_vols(nifti)  Given a path to a 4D nifti file, count the number of volumes and generate a list of all volumes
        nb_vols = spm_select_get_nbframes(nifti);
        out = cellstr(strcat(repmat(nifti, nb_vols, 1), ',', num2str([1:nb_vols]')));
        out = cellfun(@(a) a(~isspace(a)),out,'UniformOutput',false);
    end
end