function FDs = calculateFD(motion)
%Calculates framewise displacement / based on Powers et al., 2012
%code snippets from Cyril Pernet used (github.com/Cpernet/spmup/blob/master/QA/spmup_FD.m)
radius = 50; %assume head is sphere with x radius
motion = spm_detrend(motion,1);
motion(:,[4:6]) = motion(:,[4:6]).*radius;
D = diff(motion,1,1);
D = [zeros(1,6); D];
FDs = sum(abs(D),2);
end

