function firstLevel(preprocdatadir,sub,designmatdir,contrastdir,outputdir,SPM12path)

if isempty(SPM12path)
    error('be sure to add the SPM12 path in the ''runFirstLevel'' script!')
end

addpath(SPM12path)
addpath(genpath('functions'))

spm('defaults', 'FMRI');
spm_jobman('initcfg');

subjdirfun = [preprocdatadir sub '/functional/'];
subjdirstr = [preprocdatadir sub '/structural/'];

if exist([outputdir '/SPM.mat'])
    delete([outputdir '/SPM.mat'])
end

ims = cellstr(spm_select('expand',[subjdirfun 'swrasub-' sub '_task-pinf_bold.nii']));

DM = cellstr([designmatdir sub '.mat']);
load(cell2mat(DM))
clear matlabbatch

% mov parameters
[pars,dirs]=spm_select('List',subjdirfun,'^r.*\.txt$');
pars2 = [subjdirfun pars];
pars3 = cellstr(pars2);

FDs = calculateFD(importdata(cell2mat(pars3)));

[regressorFile,motionCensorMAT] = makeMotionCensorFile(FDs,outputdir,0.9);
regressorFile = cellstr(regressorFile);
matlabbatch{1}.spm.stats.fmri_spec.dir = {outputdir};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess.scans = ims;
matlabbatch{1}.spm.stats.fmri_spec.sess.multi = DM;
matlabbatch{1}.spm.stats.fmri_spec.sess.regress = struct('name', {}, 'val', {});

if isempty(motionCensorMAT)
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = {''};
else
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = regressorFile;
end

matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = 128;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {'data/cerebrum.nii,1'};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 1];

matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr([outputdir '/SPM.mat']);
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

spm_jobman('serial',matlabbatch)
clear matlabbatch

load([contrastdir sub])

matlabbatch{1}.spm.stats.con.spmmat = cellstr([outputdir '/SPM.mat']);

spm_jobman('run', matlabbatch);
end