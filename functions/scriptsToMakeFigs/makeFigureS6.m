PPIsafe_low = PPIsafe(splitSubstNaive([1:4,6:31])); %sub 5 excluded from this analysis due to trial number
PPIsafe_high = PPIsafe(splitSubstExposed([1:4,6:31]));
PPIrisky_low = PPIrisky(splitSubstNaive(1:31));
PPIrisky_high = PPIrisky(splitSubstExposed(1:31));

disp(' ')
disp('PPI analysis:')
figure; subplot(1,2,1); hold on;
param_lo = PPIsafe_low;
param_hi = PPIsafe_high;
violinMOtwoGroups(param_lo,param_hi,'b')
[~,p,~,stats] = ttest2(param_lo,param_hi);
disp(['safe PPI (in dmPFC ROI) between groups: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])

subplot(1,2,2); hold on;
param_lo = PPIrisky_low;
param_hi = PPIrisky_high;
violinMOtwoGroups(param_lo,param_hi,'r')
[~,p,~,stats] = ttest2(param_lo,param_hi);
disp(['risky PPI (in dmPFC ROI) between groups: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])
