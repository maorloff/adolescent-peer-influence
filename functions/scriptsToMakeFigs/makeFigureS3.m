figure; set(gcf,'Position',[ 300 700 1000 400]);
subplot(1,2,1); hold on; axis square
title('substance-naive')
plotPhiLine(PsafeByPhiSolo(splitSubstNaive,:),'k')
plotPhiLine(PsafeByPhiSafe(splitSubstNaive,:),'b')
plotPhiLine(PsafeByPhiRisky(splitSubstNaive,:),'r')
plotPhiLine(PsafeByPhiMix(splitSubstNaive,:),[134 15 143] ./ 255)

plot([0.4 0.9],[0.5 0.5],'k--')
xticks([0.4:0.1:0.9])

subplot(1,2,2); hold on; axis square
title('substance-exposed')
plotPhiLine(PsafeByPhiSolo(splitSubstExposed,:),'k')
plotPhiLine(PsafeByPhiSafe(splitSubstExposed,:),'b')
plotPhiLine(PsafeByPhiRisky(splitSubstExposed,:),'r')
plotPhiLine(PsafeByPhiMix(splitSubstExposed,:),[134 15 143] ./ 255)

plot([0.4 0.9],[0.5 0.5],'k--')
xticks([0.4:0.1:0.9])

function plotPhiLine(Y,color)
PhiVals = [0.4:0.1:0.9];
errorbar(PhiVals, mean(Y), SEM(Y),'color',color,'LineWidth',2)
end