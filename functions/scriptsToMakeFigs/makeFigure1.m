figure; set(gcf,'Position',[ 300 700 1000 420]);

%% figure 1b, substance-naive adolescents have larger OCU than at-risk adolescents for safe peers
%OCUsafe, transformed
subplot(1,2,1); hold on;
param1 = OCUsafe_trans(anySubUse==0);
param2 = OCUsafe_trans(anySubUse==1);
violinMOtwoGroups(param1,param2,'b')

%OCUsafe, transformed
param1 = OCUrisky_trans(anySubUse==0);
param2 = OCUrisky_trans(anySubUse==1);
violinMOtwoGroups(param1,param2,'r',1)

%adjust plot
set(gca,'xtick',[])
plot([0 8],[0.5 0.5],'k--')
xlim([0 8])
title('OCUtransformed','FontSize',16)
axis square
ylim([0 1])

%% figure 1c, OCU bias is correlated with conformity bias
disp('FIGURE 1C')
subplot(1,2,2); hold on;
ocuBiasSubstNaive = OCUsafe_trans(anySubUse==0) - OCUrisky_trans(anySubUse==0);
ocuBiasAtRisk = OCUsafe_trans(anySubUse==1) - OCUrisky_trans(anySubUse==1);
confBiasSubstNaive = conformityBias(anySubUse==0);
confBiasAtRisk = conformityBias(anySubUse==1);

xlim([-1 1]),ylim([-1 1])
plot(xlim,[0 0],'k--'); plot([0 0],ylim,'k--')
plot(confBiasSubstNaive, ocuBiasSubstNaive,'.','MarkerSize',smallMarkerSize,'color',substNaiveColor)
plot(confBiasAtRisk, ocuBiasAtRisk,'.','MarkerSize',smallMarkerSize,'color',substExposedColor)
disp('OCUsafe-OCUrisky and confSafe-confRisky')
[r,p] = corr([confBiasSubstNaive;confBiasAtRisk], [ocuBiasSubstNaive;ocuBiasAtRisk]);
disp(['r = ' num2str(round(r,3,'significant')) ', P = ' num2str(round(p,3,'significant'))])
[r,p] = corr(confBiasSubstNaive, ocuBiasSubstNaive);
disp(['substance-naive only: r = ' num2str(round(r,4,'significant')) ', P = ' num2str(round(p,4,'significant'))])
[r,p] = corr(confBiasAtRisk, ocuBiasAtRisk);
disp(['at-risk only: r = ' num2str(round(r,4,'significant')) ', P = ' num2str(round(p,4,'significant'))])
axis square
