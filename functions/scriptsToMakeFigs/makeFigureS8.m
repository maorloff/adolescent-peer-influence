figure; hold on;

disp(' ')
disp('comparing mixed model (3dMEMA in AFNI) to random effects model (SPM):')
for i = 1:2
    if i == 1
        df = 29;
        currColor = 'b';
        disp('safe:')
        AFNIdata = AFNIdataOCUsafe;
        SPMdata = SPMdataOCUsafe;
        voxelsSocialValue = voxelsSocialValueOCUsafe;
    else
        df = 29;
        currColor = 'r';
        disp('risky:')
        AFNIdata = AFNIdataOCUrisky;
        SPMdata = SPMdataOCUrisky;
        voxelsSocialValue = voxelsSocialValueOCUrisky;
    end

    AFNIcensorMask = AFNIdata==0;
    SPMcensorMask = SPMdata==0;
    
    AFNIdata(AFNIcensorMask | SPMcensorMask) = [];
    SPMdata(AFNIcensorMask | SPMcensorMask) = [];
    voxelsSocialValue(AFNIcensorMask | SPMcensorMask) = [];
    
    t = tinv(0.025,df);
    
    AFNIdataSig = AFNIdata < t | AFNIdata > -t;
    SPMdataSig = SPMdata < t | SPMdata > -t;
    
    plotAsSignificant = AFNIdataSig | SPMdataSig;

    currVoxels = voxelsSocialValue;
    scatter(AFNIdata(plotAsSignificant==0 & currVoxels==1),SPMdata(plotAsSignificant==0 & currVoxels==1),'MarkerFaceColor', ...
        currColor,'MarkerEdgeColor', currColor,'MarkerFaceAlpha',0.25,'MarkerEdgeAlpha',0.25)
    scatter(AFNIdata(plotAsSignificant==1 & currVoxels==1),SPMdata(plotAsSignificant==1 & currVoxels==1),'MarkerFaceColor', ...
        currColor,'MarkerEdgeColor', currColor,'MarkerFaceAlpha',1,'MarkerEdgeAlpha',1)
    
    axis square
    yLimits = [min([-1,floor(t)]) 5];
    xLimits = [min([-1,floor(t)]) 5];
    
    xlim([max([xLimits(1) yLimits(1)]) max([xLimits(2) yLimits(2)])])
    ylim([max([xLimits(1) yLimits(1)]) max([xLimits(2) yLimits(2)])])
    
    yLimits = ylim;
    xLimits = xlim;
    
    if i == 1
        plot([t t],yLimits,'k--','LineWidth',1.5)
        plot([-t -t],yLimits,'k--','LineWidth',1.5)
        plot(xLimits,[t t],'k--','LineWidth',1.5)
        plot(xLimits,[-t -t],'k--','LineWidth',1.5)
    end
    avgTstat = mean(AFNIdata(voxelsSocialValue==1));
    disp(['t test average for group difference: t(29) = ' num2str(avgTstat) ', P = ' num2str(2* (1 - tcdf(abs(avgTstat),29)))]);
    
    [r,p]= corr(AFNIdata(voxelsSocialValue==1 ),SPMdata(voxelsSocialValue==1 ));
    disp(['correlation across ROI voxels: r = ' num2str(r) ', P = ' num2str(p)]);
end
