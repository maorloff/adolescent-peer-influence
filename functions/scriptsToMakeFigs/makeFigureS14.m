disp(' ')
disp('age analysis:')

figNum = 1 + length(findobj('type','fig'));
figure(figNum); set(gcf,'Position',[779 964 1400 313]); subplot(1,3,1); hold on;

Xlimit = [0 12];
errorbar([1 2 3], [mean(safeSolo(age==15)) mean(mixSolo(age==15)) mean(riskySolo(age==15))],[SEM(safeSolo(age==15)) SEM(mixSolo(age==15)) SEM(riskySolo(age==15))],'color',color15,'LineWidth',2,'CapSize',0)
errorbar([5 6 7], [mean(safeSolo(age==16)) mean(mixSolo(age==16)) mean(riskySolo(age==16))],[SEM(safeSolo(age==16)) SEM(mixSolo(age==16)) SEM(riskySolo(age==16))],'color',color16,'LineWidth',2,'CapSize',0)
errorbar([9 10 11], [mean(safeSolo(age==17)) mean(mixSolo(age==17)) mean(riskySolo(age==17))],[SEM(safeSolo(age==17)) SEM(mixSolo(age==17)) SEM(riskySolo(age==17))],'color',color17,'LineWidth',2,'CapSize',0)

scatter([1 2 3], [mean(safeSolo(age==15)) mean(mixSolo(age==15)) mean(riskySolo(age==15))],largeMarkerSize,'filled','MarkerFaceColor',color15,'MarkerEdgeColor',color15)
scatter([5 6 7], [mean(safeSolo(age==16)) mean(mixSolo(age==16)) mean(riskySolo(age==16))],largeMarkerSize,'filled','MarkerFaceColor',color16,'MarkerEdgeColor',color16)
scatter([9 10 11], [mean(safeSolo(age==17)) mean(mixSolo(age==17)) mean(riskySolo(age==17))],largeMarkerSize,'filled','MarkerFaceColor',color17,'MarkerEdgeColor',color17)

xlim(Xlimit)
plot(Xlimit,[0 0],'k--')
xticks([0:12])
ylim([-0.15 0.15])
yticks([-0.15:0.05:0.15])
title('model-agnostic behavior','FontSize',16)
anova1(safeSolo,age);
anova1(mixSolo,age);
anova1(riskySolo,age);

figure(figNum); subplot(1,3,2); hold on;
currParam = riskPrefFromOCU2model(age==15);
distributionPlot(currParam,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',1)
plotSpread(currParam,'distributionColors',color15,'distributionMarkers','.','spreadWidth',1.5,'xValues',1)
plot(1 + [-lineWidth lineWidth], mean(currParam)*[1 1],'k','LineWidth',5);

currParam = riskPrefFromOCU2model(age==16);
distributionPlot(currParam,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',2.5)
plotSpread(currParam,'distributionColors',color16,'distributionMarkers','.','spreadWidth',1.5,'xValues',2.5)
plot(2.5 + [-lineWidth lineWidth], mean(currParam)*[1 1],'k','LineWidth',5);
set(gca,'xtick',[])
ylim([0 1])

currParam = riskPrefFromOCU2model(age==17);
distributionPlot(currParam,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',4)
plotSpread(currParam,'distributionColors',color17,'distributionMarkers','.','spreadWidth',1.5,'xValues',4)
plot(4 + [-lineWidth lineWidth], mean(currParam)*[1 1],'k','LineWidth',5);
set(gca,'xtick',[])
ylim([0 1])

anova1(riskPrefFromOCU2model,age);
[r,p] = corr(age,riskPrefFromOCU2model);
disp(['correlation between age and risk preference (using age as continuous measure), r = ' num2str(round(r,4,'significant')) ', P = ' num2str(round(p,4,'significant')) ])

figure(figNum); subplot(1,3,3); hold on;
violinMOthreeGroups(OCUsafe_trans(age==15),OCUsafe_trans(age==16),OCUsafe_trans(age==17),'b');
violinMOthreeGroups(OCUrisky_trans(age==15),OCUrisky_trans(age==16),OCUrisky_trans(age==17),'r',1)
ylim([0 1])
currXlim = xlim;
plot(currXlim,[0.5 0.5],'--k','LineWidth',2)
anova1(OCUsafe_trans,age);
anova1(OCUrisky_trans,age);
