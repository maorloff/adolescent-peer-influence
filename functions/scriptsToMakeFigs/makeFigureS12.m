disp(' ')
disp('correlations between risk preference and OCU')
conditions = {'safe','risky'};
for currCondition = 1:2
    figure; hold on; ylim([0 1])
    for subjIndex=[0 1]
        if subjIndex==0
            currRho = riskPrefFromOCU2model(splitSubstNaive);
            currGroup = 'substance-naive';
        elseif subjIndex==1
            currRho = riskPrefFromOCU2model(splitSubstExposed);
            currGroup = 'substance-exposed';
        end
        switch currCondition
            case 1
                currOCUall = OCUsafe_trans;
                if subjIndex == 0
                    currColor = colorBlueDrugNo;
                    currOCU = OCUsafe_trans(splitSubstNaive);
                elseif subjIndex == 1
                    currColor = colorBlueDrugYes;
                    currOCU = OCUsafe_trans(splitSubstExposed);
                end
            case 2
                currOCUall = OCUrisky_trans;
                if subjIndex == 0
                    currColor = colorRedDrugNo;
                    currOCU = OCUrisky_trans(splitSubstNaive);
                elseif subjIndex == 1
                    currColor = colorRedDrugYes;
                    currOCU = OCUrisky_trans(splitSubstExposed);
                end
        end
        [r, p]=corr(currRho, currOCU);
        disp(['risk preference and OCU' conditions{currCondition} ' for ' currGroup ': r = ' num2str(round(r,4,'significant')) ', P = ' num2str(round(p,4,'significant'))])
        plot(currRho, currOCU,'.','color',currColor,'MarkerSize',30)
        tempMinMax=[min(currRho) max(currRho)];
        stats=regstats(currOCU, currRho);
        plot(tempMinMax, stats.tstat.beta(1)+stats.tstat.beta(2)*tempMinMax,'color',currColor,'LineWidth',2)
    end
    plot([0 0.9],[0.5 0.5],'k--','LineWidth',1.5)
    [r, p]=corr(riskPrefFromOCU2model, currOCUall);
    disp(['risk preference and OCU' conditions{currCondition} ' all together: r = ' num2str(round(r,4,'significant')) ', P = ' num2str(round(p,4,'significant'))])
end
