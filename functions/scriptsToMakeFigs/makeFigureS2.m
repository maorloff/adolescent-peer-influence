figure; hold on;
Xlimit = [0 8];
errorbar([1 2 3], [mean(safeSolo(splitSubstNaive)) mean(mixSolo(splitSubstNaive)) mean(riskySolo(splitSubstNaive))],[SEM(safeSolo(splitSubstNaive)) SEM(mixSolo(splitSubstNaive)) SEM(riskySolo(splitSubstNaive))],'color',substNaiveColor,'LineWidth',2,'CapSize',0)
errorbar([5 6 7], [mean(safeSolo(splitSubstExposed)) mean(mixSolo(splitSubstExposed)) mean(riskySolo(splitSubstExposed))],[SEM(safeSolo(splitSubstExposed)) SEM(mixSolo(splitSubstExposed)) SEM(riskySolo(splitSubstExposed))],'color',substExposedColor,'LineWidth',2,'CapSize',0)
scatter([1 2 3], [mean(safeSolo(splitSubstNaive)) mean(mixSolo(splitSubstNaive)) mean(riskySolo(splitSubstNaive))],largeMarkerSize,'filled','MarkerFaceColor',substNaiveColor,'MarkerEdgeColor',substNaiveColor)
scatter([5 6 7], [mean(safeSolo(splitSubstExposed)) mean(mixSolo(splitSubstExposed)) mean(riskySolo(splitSubstExposed))],largeMarkerSize,'filled','MarkerFaceColor',substExposedColor,'MarkerEdgeColor',substExposedColor)
xlim(Xlimit)
plot(Xlimit,[0 0],'k--')
title('model-agnostic behavior','FontSize',16)

anova_rm({[safeSolo(splitSubstNaive) riskySolo(splitSubstNaive)] [safeSolo(splitSubstExposed) riskySolo(splitSubstExposed)]});

anova_rm({[safeSolo(splitSubstNaive) mixSolo(splitSubstNaive) riskySolo(splitSubstNaive)] [safeSolo(splitSubstExposed) mixSolo(splitSubstExposed) riskySolo(splitSubstExposed)]});

%stats
disp(' ')
disp('stats for model-agnostic behavior:')
p=anova_rm([safeSolo(splitSubstNaive),mixSolo(splitSubstNaive),riskySolo(splitSubstNaive)],'on');
disp(['substance-naive repeated-measures ANOVA: P = ' num2str(round(p(1),4,'significant'))])
p=anova_rm([safeSolo(splitSubstExposed),mixSolo(splitSubstExposed),riskySolo(splitSubstExposed)],'on');
disp(['substance-exposed repeated-measures ANOVA: P = ' num2str(round(p(1),4,'significant'))])

[~,p,~,stats] = ttest(safeSolo(splitSubstNaive),0);
disp(['substance-naive different from 0, safe: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])
[~,p,~,stats] = ttest(safeSolo(splitSubstExposed),0);
disp(['substance-exposed different from 0, safe: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])


[~,p,~,stats] = ttest2(safeSolo(splitSubstNaive),safeSolo(splitSubstExposed),'Tail','right');
disp(['safe between groups: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant')) ', one-tailed'])

[~,p,~,stats] = ttest2(mixSolo(splitSubstNaive),mixSolo(splitSubstExposed));
disp(['mix between groups: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])

[~,p,~,stats] = ttest2(riskySolo(splitSubstNaive),riskySolo(splitSubstExposed));
disp(['risky between groups: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])
