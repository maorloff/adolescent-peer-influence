contrastList = {'OCU','socialInfo'};

rewardRelated = {'reward','value','utility','dopaminergic','gain','gains','monetary','money','rewarding','rewards', ...
    'reward anticipation','subjective','values'};

socialRelated = {'social','social cognition', 'social cognitive','social interaction','social interactions','theory mind','tom'};

figCount = length(findobj('type','figure'));
for j = 1:length(contrastList)
    conditionList = {'Safe', 'Risky'};
    for i = 1:length(conditionList)
        if j == 1 & i == 2
            continue %skip OCUrisky
        end
        figCount = figCount+1;
        
        if strcmp(contrastList{j},'OCU')
            resultsFile = ['drugDiffThresh005results.txt'];
        else
            resultsFile = ['Thresh005results.txt'];
        end
        
        try
            [labels{i,j}, nsResults{i,j}] = importNSdecode(['data/reran' contrastList{j} conditionList{i} resultsFile]);
        catch
            [labels{i,j}, nsResults{i,j}] = importNSdecode(['data/' contrastList{j} conditionList{i} resultsFile]);
        end
        %% get decoder vals to plot in histogram
        switch contrastList{j}
            case 'OCU'
                allMeans{i,j} = nsResults{i,j}(:,1);
            case 'socialInfo'
                noUseMeans{i,j} = nsResults{i,j}(:,1);
                subUseMeans{i,j} = nsResults{i,j}(:,2);
        end
        %% plot distribution of means
        switch contrastList{j}
            case 'OCU'
                xlimToSet = [-0.2 0.2];
                currLabelsToPlot = rewardRelated;
            case 'socialInfo'
                xlimToSet = [-0.3 0.3];
                ylimToSet = [0 160];
                currLabelsToPlot = socialRelated;
            otherwise
                xlimToSet = [-1 1];
        end
        transparencySetting = 0.5;
        histogramEdges = linspace(xlimToSet(1),xlimToSet(2),150);
        
        figure(figCount); set(gcf, 'Position', get(0, 'Screensize'));
        switch contrastList{j}
            case 'OCU'
                subplot(3,1,1); hold on;
                title({conditionList{i},contrastList{j}},'FontSize',16)
                histogram(allMeans{i,j},histogramEdges,'FaceColor',allSubColor,'EdgeColor',allSubColor,'FaceAlpha',transparencySetting,'EdgeAlpha',transparencySetting)
                plotLabels(currLabelsToPlot,labels{i,j},allMeans{i,j})
                xlim(xlimToSet)
                set(gca,'xtick',[-0.3:0.1:0.3])
            case 'socialInfo'
                subplot(3,1,2); hold on;
                title({conditionList{i},contrastList{j}},'FontSize',16)
                histogram(noUseMeans{i,j},histogramEdges,'FaceColor',substNaiveColor,'EdgeColor',substNaiveColor,'FaceAlpha',transparencySetting,'EdgeAlpha',transparencySetting)
                ylim(ylimToSet)
                plotLabels(currLabelsToPlot,labels{i,j},noUseMeans{i,j})
                xlim(xlimToSet)
                set(gca,'xtick',[-0.3:0.1:0.3])
                
                subplot(3,1,3); hold on;
                histogram(subUseMeans{i,j},histogramEdges,'FaceColor',substExposedColor,'EdgeColor',substExposedColor,'FaceAlpha',transparencySetting,'EdgeAlpha',transparencySetting)
                ylim(ylimToSet)
                plotLabels(currLabelsToPlot,labels{i,j},subUseMeans{i,j})
                xlim(xlimToSet)
                set(gca,'xtick',[-0.3:0.1:0.3])
        end
    end
end

function index = findTermIndex(termToFind,labels)
for i = 1:length(labels)
    if strcmp(termToFind, labels(i))
        index = i;
        break
    end
end
if ~exist('index')
    error('something''s wrong, maybe the label doesn''t exist...?')
end
end

function plotLabels(labelsToPlot,labels,decodeVals)
currYlim = ylim;
labelSpread = linspace(currYlim(1),currYlim(2),length(labelsToPlot)+2);
labelSpread = labelSpread([2:end-1]);

for i = 1:length(labelsToPlot)
    labelLoc(i) = findTermIndex(labelsToPlot{i},labels);
end
[~,I] = sort(decodeVals(labelLoc));
labelSpread = labelSpread(I);
labelSpread = [labelSpread(1:2:end) labelSpread(2:2:end)];
plot([decodeVals(labelLoc) decodeVals(labelLoc)],currYlim,'k--')
for i = 1:length(labelsToPlot)
    text(decodeVals(labelLoc(i)), labelSpread(i), [char(8212) labelsToPlot{i}],'FontName','Arial','FontSize',30)
end
end
