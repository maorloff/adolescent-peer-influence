disp(' ')
disp('neural substrates of risk:')
RP = riskPrefFromOCU2model(1:31);

figure; set(gcf,'Position',[1000 918 270 420]); hold on;
param_lo = dACC_pmodCV(splitSubstNaive(1:31));
param_hi = dACC_pmodCV(splitSubstExposed(1:31));
distributionPlot(param_lo,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1,'xValues',[1])
distributionPlot(gca,param_hi,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',[2.5])
plotSpread(param_lo,'distributionColors',substNaiveColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[1])
plotSpread(param_hi,'distributionColors',substExposedColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[2.5])
plot(1+[-0.6 0.6], mean(param_lo)*[1 1],'k','LineWidth',5)
plot(2.5+[-0.6 0.6], mean(param_hi)*[1 1],'k','LineWidth',5)
[~,p,~,stats] = ttest2(param_lo,param_hi);
disp(['between group difference in acc beta: t(' num2str(stats.df) ') = ' num2str(round(stats.tstat,4,'significant')) ' P = ' num2str(round(p,4,'significant'))])
xticks([])
plot(xlim,[0 0],'k--')
title('dACC pmodCV')

labels = {'dACC','rInsula'};
for i = 1:2
    currLabel = labels{i};
    switch i
        case 1
            currBetas = dACC_pmodCV(1:31);
        case 2
            currBetas = rInsula_pmodCV(1:31);
    end
    
    figure; set(gcf,'Position',[1000 918 560 420]); hold on;
    params=[RP currBetas];
    [r,p] = corr(params(:,1),params(:,2));
    disp([currLabel ' CV beta and risk preference correlation: r = ' num2str(r) ', P = ' num2str(p)])
    params=params(splitSubstNaive(1:31),:);
    stats=regstats(params(:,2), params(:,1));
    plot(params(:,1), params(:,2),'.','MarkerSize',markerSize,'color',substNaiveColor)
    plot([min(params(:,1)) max(params(:,1))], stats.tstat.beta(1) + stats.tstat.beta(2)*[min(params(:,1)) max(params(:,1))],'color',substNaiveColor,'LineWidth',2)

    params=[RP currBetas];
    [r,p] = corr(params(:,1),params(:,2));
    params=params(splitSubstExposed(1:31),:);
    stats=regstats(params(:,2), params(:,1));
    plot(params(:,1), params(:,2),'.','MarkerSize',markerSize,'color',substExposedColor)
    plot([min(params(:,1)) max(params(:,1))], stats.tstat.beta(1) + stats.tstat.beta(2)*[min(params(:,1)) max(params(:,1))],'color',substExposedColor,'LineWidth',2)
    axis square
    title(currLabel)
end


