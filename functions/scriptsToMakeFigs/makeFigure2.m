figure; set(gcf,'Position',[ 300 206 600 420]);

%% figure 2b, substance-naive adolescents have larger vmPFC signal than at-risk adolescents for safe peers
anySubUse = anySubUse(1:31); %get imaging subs

%safe
subplot(1,2,1); hold on;
param1 = safeValue(anySubUse==0);
param2 = safeValue(anySubUse==1);
violinMOtwoGroups(param1,param2,'b')

%risky
param1 = riskyValue(anySubUse==0);
param2 = riskyValue(anySubUse==1);
violinMOtwoGroups(param1,param2,'r',1)

%adjust plot
set(gca,'xtick',[])
plot([0 8],[0 0],'k--')
xlim([0 8])
title('vmPFC [-4, 40, -8]','FontSize',16)

%% figure 2c, social information signal is similar between substance-naive and at-risk adolescents
%safe
subplot(1,2,2); hold on;
param1 = safeInfo(anySubUse==0);
param2 = safeInfo(anySubUse==1);
violinMOtwoGroups(param1,param2,'b')

%risky
param1 = riskyInfo(anySubUse==0);
param2 = riskyInfo(anySubUse==1);
violinMOtwoGroups(param1,param2,'r',1)

%adjust plot
set(gca,'xtick',[])
plot([0 8],[0 0],'k--')
xlim([0 8])
title('dmPFC [2, 56, 20]','FontSize',16)
