disp(' ')
disp('gender analysis:')
figure; set(gcf,'Position',[779 964 700 313]);

Xlimit = [0 8];
errorbar([1 2 3], [mean(safeSolo(gender==1)) mean(mixSolo(gender==1)) mean(riskySolo(gender==1))],[SEM(safeSolo(gender==1)) SEM(mixSolo(gender==1)) SEM(riskySolo(gender==1))],'color',maleColor,'LineWidth',2,'CapSize',0)
errorbar([5 6 7], [mean(safeSolo(gender==0)) mean(mixSolo(gender==0)) mean(riskySolo(gender==0))],[SEM(safeSolo(gender==0)) SEM(mixSolo(gender==0)) SEM(riskySolo(gender==0))],'color',femaleColor,'LineWidth',2,'CapSize',0)
scatter([1 2 3], [mean(safeSolo(gender==1)) mean(mixSolo(gender==1)) mean(riskySolo(gender==1))],largeMarkerSize,'filled','MarkerFaceColor',maleColor,'MarkerEdgeColor',maleColor)
scatter([5 6 7], [mean(safeSolo(gender==0)) mean(mixSolo(gender==0)) mean(riskySolo(gender==0))],largeMarkerSize,'filled','MarkerFaceColor',femaleColor,'MarkerEdgeColor',femaleColor)
xlim(Xlimit)
plot(Xlimit,[0 0],'k--')
title('model-agnostic behavior','FontSize',16)

[~,p,~,stats] = ttest2(safeSolo(gender==1),safeSolo(gender==0));
disp(['safe influence between genders: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])

[~,p,~,stats] = ttest2(mixSolo(gender==1),mixSolo(gender==0));
disp(['mix influence between gender: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])

[~,p,~,stats] = ttest2(riskySolo(gender==1),riskySolo(gender==0));
disp(['risky influence between gender: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])

subplot(1,2,1); hold on;

currParam = riskPrefFromOCU2model(gender==1);
distributionPlot(currParam,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',1)
plotSpread(currParam,'distributionColors',maleColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',1)
plot(1 + [-lineWidth lineWidth], mean(currParam)*[1 1],'k','LineWidth',5);

currParam = riskPrefFromOCU2model(gender==0);
distributionPlot(currParam,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',2.5)
plotSpread(currParam,'distributionColors',femaleColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',2.5)
plot(2.5 + [-lineWidth lineWidth], mean(currParam)*[1 1],'k','LineWidth',5);
set(gca,'xtick',[])
ylim([0 1])
[~,p,~,stats] = ttest2(riskPrefFromOCU2model(gender==1),riskPrefFromOCU2model(gender==0));
disp(['risk preference between gender: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])

subplot(1,2,2); hold on;
violinMOtwoGroups(OCUsafe_trans(gender==1),OCUsafe_trans(gender==0),'b')
violinMOtwoGroups(OCUrisky_trans(gender==1),OCUrisky_trans(gender==0),'r',1)
ylim([0 1])
currXlim = xlim;
plot(currXlim,[0.5 0.5],'--k','LineWidth',2)

[~,p,~,stats] = ttest2(OCUsafe_trans(gender==1),OCUsafe_trans(gender==0));
disp(['OCUsafe between gender: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])
[~,p,~,stats] = ttest2(OCUrisky_trans(gender==1),OCUrisky_trans(gender==0));
disp(['OCUrisky between gender: t(' num2str(stats.df) ') = ' num2str(stats.tstat) ', P = ' num2str(round(p,4,'significant'))])
