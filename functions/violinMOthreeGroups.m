function violinMOtwoGroups( param_lo, param_med, param_hi, currColor, continuePlot )
addpath('/Volumes/pinfadol/behavReplication/scripts/behavior/distributionPlot')
addpath('/Volumes/pinfadol/scripts')
locJitter = 0;
lineWidth = 0.6;
if ~exist('continuePlot','var')
    continuePlot = 0;
end
if continuePlot
    locJitter = 7;
end
if ~exist('currColor','var')
    currColor = [rand rand rand];
end
distributionPlot(param_lo,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1,'xValues',[locJitter+1])
distributionPlot(gca,param_med,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',[locJitter+2.5])
distributionPlot(gca,param_hi,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',[locJitter+4])

plotSpread(param_lo,'distributionColors',currColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[locJitter+1])
plotSpread(param_med,'distributionColors',currColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[locJitter+2.5])
plotSpread(param_hi,'distributionColors',currColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[locJitter+4])

plot(locJitter+1+[-lineWidth lineWidth], mean(param_lo)*[1 1],'k','LineWidth',5);
plot(locJitter+2.5+[-lineWidth lineWidth], mean(param_med)*[1 1],'k','LineWidth',5);
plot(locJitter+4+[-lineWidth lineWidth], mean(param_hi)*[1 1],'k','LineWidth',5);

% [~,p,~,stats] = ttest2(param_lo,param_hi);
% d = cohenDmo(param_lo,param_hi);
% disp(['t(' num2str(stats.df) ') = ' num2str(round(stats.tstat,4,'significant')) ' P = ' num2str(round(p,4,'significant')) ', Cohen''s d = ' num2str(round(d,4,'significant'))])


set(gca,'xtick',[])
end

