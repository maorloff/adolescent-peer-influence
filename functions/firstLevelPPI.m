function firstLevelPPI(preprocdatadir,sub,firstleveldatadir,outputdir,SPM12path)

if isempty(SPM12path)
    error('be sure to add the SPM12 path in the ''runFirstLevel'' script!')
end

addpath(SPM12path)
addpath(genpath('functions'))

spm('defaults', 'FMRI');
spm_jobman('initcfg');

if exist([outputdir '/SPM.mat'])
    delete([outputdir '/SPM.mat'])
end

subjdirfun = [preprocdatadir sub '/functional/'];
startingFolder = pwd;

%% extract time series
matlabbatch{1}.spm.util.voi.spmmat = {[firstleveldatadir 'SPM.mat']};
matlabbatch{1}.spm.util.voi.adjust = 1;
matlabbatch{1}.spm.util.voi.session = 1;
matlabbatch{1}.spm.util.voi.name = 'vmPFC_r6_sphere';

matlabbatch{1}.spm.util.voi.roi{1}.spm.spmmat = {''};
matlabbatch{1}.spm.util.voi.roi{1}.spm.contrast = 1;
matlabbatch{1}.spm.util.voi.roi{1}.spm.conjunction = 1;
matlabbatch{1}.spm.util.voi.roi{1}.spm.threshdesc = 'none';
matlabbatch{1}.spm.util.voi.roi{1}.spm.thresh = 1;
matlabbatch{1}.spm.util.voi.roi{1}.spm.extent = 0;

matlabbatch{1}.spm.util.voi.roi{2}.sphere.centre = [-3 50 -17];
matlabbatch{1}.spm.util.voi.roi{2}.sphere.radius = 6;
matlabbatch{1}.spm.util.voi.roi{2}.sphere.move.local.spm = 1;
matlabbatch{1}.spm.util.voi.expression = 'i1 & i2';

save([outputdir '/time_series'], 'matlabbatch');
spm_jobman('run', matlabbatch);
clear matlabbatch

%% deconvolve
matlabbatch{1}.spm.stats.ppi.spmmat = {'SPM.mat'};

matlabbatch{1}.spm.stats.ppi.type.ppi.voi = {['VOI_vmPFC_r6_sphere_1.mat']};
matlabbatch{1}.spm.stats.ppi.type.ppi.u = [3 1 1; 4 1 -1];
matlabbatch{1}.spm.stats.ppi.name = ['mPFCxConf_r6_sphere'];
matlabbatch{1}.spm.stats.ppi.disp = 0;

spm_jobman('run', matlabbatch);
clear matlabbatch
cd(startingFolder)

%% run GLM with deconvolved regressors
load([firstleveldatadir 'PPI_mPFCxConf_r6_sphere'])
ims = cellstr(spm_select('expand',[subjdirfun 'swrasub-' sub '_task-pinf_bold.nii']));
clear matlabbatch

% mov parameters
[pars,dirs]=spm_select('List',subjdirfun,'^r.*\.txt$');
pars2 = [subjdirfun pars];
pars3 = cellstr(pars2);

FDs = calculateFD(importdata(cell2mat(pars3)));

[regressorFile,motionCensorMAT] = makeMotionCensorFile(FDs,outputdir,0.9);
regressorFile = cellstr(regressorFile);
matlabbatch{1}.spm.stats.fmri_spec.dir = {outputdir};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess.scans = ims;
matlabbatch{1}.spm.stats.fmri_spec.sess.multi = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess.regress(1).name = 'ppi';
matlabbatch{1}.spm.stats.fmri_spec.sess.regress(1).val = PPI.ppi;
matlabbatch{1}.spm.stats.fmri_spec.sess.regress(2).name = 'ppi_y';
matlabbatch{1}.spm.stats.fmri_spec.sess.regress(2).val = PPI.Y;
matlabbatch{1}.spm.stats.fmri_spec.sess.regress(3).name = 'ppi_psyc';
matlabbatch{1}.spm.stats.fmri_spec.sess.regress(3).val = PPI.P;

if isempty(motionCensorMAT)
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = {''};
else
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = regressorFile;
end

matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = 128;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];

matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr([outputdir '/SPM.mat']);
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

spm_jobman('serial',matlabbatch)
clear matlabbatch

matlabbatch{1}.spm.stats.con.spmmat = cellstr([outputdir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'PPI_vmPFC';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.convec = 1; % paired t-test. t-test=[1 -1]
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 0;
spm_jobman('serial',matlabbatch)

end

