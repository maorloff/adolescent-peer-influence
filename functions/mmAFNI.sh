#!/bin/bash

currContrast=$1

source ./functions/@filePathsSecondLevel.sh $currContrast

3dMEMA -prefix ${saveName}/MM -verb 1 \
	-jobs 8 \
	-groups subUse noUse \
	-set subUse \
		930100 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930100/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930100/OCU${currContrast}+tlrc'[1]' \
		930107 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930107/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930107/OCU${currContrast}+tlrc'[1]' \
		930108 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930108/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930108/OCU${currContrast}+tlrc'[1]' \
		930109 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930109/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930109/OCU${currContrast}+tlrc'[1]' \
		930110 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930110/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930110/OCU${currContrast}+tlrc'[1]' \
		930113 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930113/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930113/OCU${currContrast}+tlrc'[1]' \
		930117 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930117/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930117/OCU${currContrast}+tlrc'[1]' \
		930118 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930118/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930118/OCU${currContrast}+tlrc'[1]' \
		930121 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930121/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930121/OCU${currContrast}+tlrc'[1]' \
		930126 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930126/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930126/OCU${currContrast}+tlrc'[1]' \
		930128 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930128/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930128/OCU${currContrast}+tlrc'[1]' \
		930129 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930129/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930129/OCU${currContrast}+tlrc'[1]' \
		930132 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930132/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930132/OCU${currContrast}+tlrc'[1]' \
		930133 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930133/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930133/OCU${currContrast}+tlrc'[1]' \
		930135 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930135/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930135/OCU${currContrast}+tlrc'[1]' \
		930136 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930136/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930136/OCU${currContrast}+tlrc'[1]' \
	-set noUse \
		930101 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930101/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930101/OCU${currContrast}+tlrc'[1]' \
		930102 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930102/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930102/OCU${currContrast}+tlrc'[1]' \
		930103 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930103/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930103/OCU${currContrast}+tlrc'[1]' \
		930104 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930104/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930104/OCU${currContrast}+tlrc'[1]' \
		930112 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930112/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930112/OCU${currContrast}+tlrc'[1]' \
		930114 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930114/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930114/OCU${currContrast}+tlrc'[1]' \
		930115 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930115/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930115/OCU${currContrast}+tlrc'[1]' \
		930116 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930116/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930116/OCU${currContrast}+tlrc'[1]' \
		930119 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930119/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930119/OCU${currContrast}+tlrc'[1]' \
		930120 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930120/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930120/OCU${currContrast}+tlrc'[1]' \
		930123 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930123/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930123/OCU${currContrast}+tlrc'[1]' \
		930124 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930124/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930124/OCU${currContrast}+tlrc'[1]' \
		930125 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930125/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930125/OCU${currContrast}+tlrc'[1]' \
		930130 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930130/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930130/OCU${currContrast}+tlrc'[1]' \
		930134 fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930134/OCU${currContrast}+tlrc'[0]' fMRIdata/firstLevelAFNI/${currContrast}SocialValue/930134/OCU${currContrast}+tlrc'[1]' \
	-unequal_variance \
	-mask data/cerebrumForAFNI.nii

3dmaskdump -o ${saveName}/OCU${currContrast}_MMvoxels.txt -noijk ${saveName}/MM+tlrc
