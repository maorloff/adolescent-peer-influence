function extractROIs(contrastpath,ROIcenters,saveName,appendCoordinates)
startingdir = pwd;
addpath(genpath('functions'))

for currROI = 1:size(ROIcenters,1)
    makeparams
    params.spmmat = [contrastpath 'SPM.mat'];
    roundedROI = roundROIs(ROIcenters(currROI,:),params.spmmat);
    ROIfilename = genROIs(params,{roundedROI},cellstr(saveName{currROI}));
    cd(startingdir)
    load([contrastpath 'VOI_' ROIfilename])
    
    if appendCoordinates(currROI) == 1
        varName = [saveName{currROI} '_' minusToDash(num2str(ROIcenters(currROI,1))) '_' minusToDash(num2str(ROIcenters(currROI,2))) '_' minusToDash(num2str(ROIcenters(currROI,3)))];
    else
        varName = [saveName{currROI}];
    end
    evalThis = [varName ' = Y;'];
    eval(evalThis);
    
    save('data/reranROIs',varName,'-append')
end
end
