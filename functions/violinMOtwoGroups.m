function violinMOtwoGroups( param_lo, param_hi, currColor, continuePlot )
locJitter = 0;
lineWidth = 0.6;
if ~exist('continuePlot','var')
    continuePlot = 0;
end
if continuePlot
    locJitter = 4.5;
end
if ~exist('currColor','var')
    currColor = [rand rand rand];
end
distributionPlot(param_lo,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1,'xValues',[locJitter+1])
distributionPlot(gca,param_hi,'color',[0.8 0.8 0.8],'showMM',0, 'distWidth',1, 'xValues',[locJitter+2.5])
plotSpread(param_lo,'distributionColors',currColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[locJitter+1])
plotSpread(param_hi,'distributionColors',currColor,'distributionMarkers','.','spreadWidth',1.5,'xValues',[locJitter+2.5])
plot(locJitter+1+[-lineWidth lineWidth], mean(param_lo)*[1 1],'k','LineWidth',5);
plot(locJitter+2.5+[-lineWidth lineWidth], mean(param_hi)*[1 1],'k','LineWidth',5);

set(gca,'xtick',[])
end

