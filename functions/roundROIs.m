function roundedROIs = roundROIs( ROIcenter, SPMmat )
load(SPMmat)

transMAT=SPM.xY.VY(1).mat;
imageDims=SPM.xY.VY(1).dim;

for currDim = 1:3
    startPoint = transMAT(currDim,4)+transMAT(currDim,currDim);
    endPoint = imageDims(currDim)*transMAT(currDim,currDim)+startPoint-transMAT(currDim,currDim);
    XYZvoxels{currDim} = startPoint:transMAT(currDim,currDim):endPoint;
    [~,I] = min(abs(ROIcenter(currDim)-XYZvoxels{currDim}));
    roundedROIs(currDim) = XYZvoxels{currDim}(I);
end

if sum(imageDims==[length(XYZvoxels{1}) length(XYZvoxels{2}) length(XYZvoxels{3})])~=3
    error('there was a calculation issue and the current dimensions do not match the dimensions of the input image...')
end

disp(['[' num2str(ROIcenter(1)) ', ' num2str(ROIcenter(2)) ', ' num2str(ROIcenter(3)) '] --> [ ' num2str(roundedROIs(1)) ', ' num2str(roundedROIs(2)) ', ' num2str(roundedROIs(3)) ']'])

end

