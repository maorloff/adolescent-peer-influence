function filename = genROIs(varargin)
% Function to generate multiple eigenvariate ROIs.
% FORMAT genROIs(params)
%
% Generates ROIs for every peak in chosen contrast. Note, you will have to
% interactively specify the contrast and thresholds, so be sure that they
% match with what is specified in params.
%
% params should be a structure with the following fields. 
%
% params.spmmat = 'absolute/path/to/SPM.mat'
% params.adjust = 0
% params.session = 1
% params.conjunction = 1;
% params.contrast_index = 1
% params.threshdesc = 'FWE'
% params.thresh = '0.05'
% params.extent = 0
% params. mask = struct('contrast', {}, 'thresh', {}, 'mtype', {});
% params.radius = 5;
%
% Modify to suit
% your needs. Note, only sphere VOIs currently supported, but you can tweak
% function if you need some other volume.
% 
% FORMAT genROIs(params, coord_list)
% 
% Generates ROIs for coordinates in cell array.
%
% coord_list should be cell array, e.g. {[-3 14 49], [-24,-7,55]}
%
% FORMAT genROIs(params, coord_list, region_names)
% 
% Generates ROIs for coordinates in cell array, and uses names provided in
% cell array region_names in the VOI file names.
% e.g. {'dACC', 'FrontalSupL'}
% 
% VOIs will be created in same directory as SPM.mat.

spm_jobman('initcfg');
if nargin < 1
    error('Pass in at least 2 arguments.');
end
params = varargin{1};
s = load(params.spmmat, 'SPM');
s = s.SPM;
if nargin ==  1
    coord_list = loadPeakCoords();
end
if nargin == 2
    coord_list = varargin{2};
end
if nargin <= 2
    region_names = getRegionNames(coord_list); %repmat({'REGION'}, length(coord_list), 1);
end
if nargin == 3
    coord_list = varargin{2};
    region_names = varargin{3};
    if length(coord_list) ~= length(region_names)
        error('Arguments coord_list and region_names are of unequal lengths.')
    end
end
if nargin > 3
    error('Too many arguments.')
end

% Build matlabbatch and run
for ii=1:length(coord_list)
    coord_str = coord2Str(coord_list{ii});
    contrast_name = getContrastName();
    region = region_names{ii};
    voi_dim = sprintf('%dmmSphere', params.radius);
    matlabbatch{ii}.spm.util.voi.spmmat = {params.spmmat};
    matlabbatch{ii}.spm.util.voi.adjust = params.adjust;
    matlabbatch{ii}.spm.util.voi.session = params.session;
    matlabbatch{ii}.spm.util.voi.name = strjoin({region, coord_str, contrast_name, voi_dim}, '_');
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.spmmat = {''};
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.contrast = params.contrast_index;
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.conjunction = params.conjunction;
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.threshdesc = params.threshdesc;
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.thresh = params.thresh;
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.extent = params.extent;
    matlabbatch{ii}.spm.util.voi.roi{1}.spm.mask = params.mask;
    matlabbatch{ii}.spm.util.voi.roi{2}.sphere.centre = transpose(coord_list{ii});
    matlabbatch{ii}.spm.util.voi.roi{2}.sphere.radius = params.radius;
    matlabbatch{ii}.spm.util.voi.roi{2}.sphere.move.fixed = 1;
    matlabbatch{ii}.spm.util.voi.expression = 'i1&i2';
end
spm_jobman('run', matlabbatch);
filename = matlabbatch{ii}.spm.util.voi.name;

% Helper functions

    function [coord_str] = coord2Str(coord)
        if size(coord,1) > size(coord,2)
            coord = transpose(coord);
        end
        coord_str = strrep(num2str(coord), '-', 'm');
        coord_str = regexprep(coord_str, '\s{2,}', ' ');
        coord_str = strrep(coord_str, ' ', '_');
    end

    function [contrast_name] = getContrastName()
        contrast_name = s.xCon(params.contrast_index).name;
        contrast_name = regexprep(contrast_name, '\s{2,}', ' ');
        contrast_name = regexprep(contrast_name, '\s', '_');
        contrast_name = regexprep(contrast_name, '\W', '');
    end

    function [coords] = loadPeakCoords()
        xSPM  = struct( ...
            'swd',      spm_str_manip(params.spmmat, 'H'), ...
            'title',    s.xCon(params.contrast_index).name, ...
            'Ic',       params.contrast_index...
            );
        [~, xSPM] = spm_getSPM(xSPM);
        tabDat = spm_list('Table',xSPM);
        coords = tabDat.dat(:,end);
    end

    % Older versions of matlab do not have strjoin. Here is a simple
    % version, sufficient for its intended purpose.
    function joinedStr = strjoin(c, delim)        
        if nargin ~= 2
            error('WrongNumberofArguments');
        end
        if ~iscell(c)
            error('cShouldBeCellArray');
        end
        if ~iscellstr(c)
            error('InvalidCellType');
        end
        if ~ischar(delim)
            error('UseCharForDelim');
        end
        numStrings = numel(c);
        if numStrings < 1
            joinedStr = '';
        else
            joinedStr = '';
            for strjoin_ii = 1 : numStrings-1
                joinedStr = [joinedStr c{strjoin_ii} delim];
            end
            joinedStr = [joinedStr c{end}];
        end        
    end
end

function region_labels = getRegionNames(coord_list)
% Retrieves regions for each coordinate. Based on code in xjview
X = load('TDdatabase.mat'); % Belongs to xjview
T = [...
     2     0     0   -92
     0     2     0  -128
     0     0     2   -74
     0     0     0     1];
N = numel(coord_list);
region_labels = cell(N,1);
mni = nan(N,3);
for ii = 1 : N
    mni(ii,:) = coord_list{ii};
end
index = mni2cor(round(mni./2).*2, T);
atlas_indexes = [3 6];
for ii = 1 : N
    for jj = 1 : length(atlas_indexes)
        glabels{jj} = get_label(atlas_indexes(jj), index(ii,:), X);
    end
    glabels = glabels(~strcmp(glabels, ''));
    if numel(glabels) == 0
        lbl = 'undefined';
    else
        lbl = '';
        for kk = 1 : numel(glabels)-1
            lbl = sprintf('%s%s+', lbl, glabels{kk});
        end
        lbl = sprintf('%s%s', lbl, glabels{end});
    end
    region_labels{ii} = lbl;
end
end

function g_label = get_label(atlas_index, index, X)
g = X.DB{atlas_index}.mnilist(index(1), index(2), index(3));
if g ~= 0
    g_label = X.DB{atlas_index}.anatomy{g};
else
    g_label = '';
end
g_label = strrep(g_label, ' ', '');
end

function coords = mni2cor(mni, T)
% Based on xjview
coords = [mni(:,1) mni(:,2) mni(:,3) ones(size(mni,1),1)] * transpose(inv(T));
coords(:,4) = [];
coords = round(coords);
end