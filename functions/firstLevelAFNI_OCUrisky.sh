#!/bin/bash

currSub=$1
currContrast=risky

source ./functions/@filePathsFirstLevel.sh $currSub $currContrast

imageFile=$(ls ${subDir}/swra*.nii)

3drefit -TR 2 $imageFile

3dDeconvolve -GOFORIT 9 -overwrite -bucket "${saveDir}/decon" -input "${imageFile}" -censor "${censorFile}" -polort A \
        -num_stimts 10 \
        -stim_label 1 'Group_CueP1' -stim_times 1 ${stimFile}_1.1D 'BLOCK4(0.25,1)' \
        -stim_label 2 'Group_ViewP1P2_SafeInf' -stim_times 2 ${stimFile}_2.1D 'BLOCK4(6,1)' \
        -stim_label 3 'Group_ViewP1P2_RiskyInfConf' -stim_times 3 ${stimFile}_3.1D 'BLOCK4(6,1)' \
        -stim_label 4 'Group_ViewP1P2_RiskyInfNoConf' -stim_times 4 ${stimFile}_4.1D 'BLOCK4(6,1)' \
        -stim_label 5 'GroupViewP1P2_MixInfRiskyConf' -stim_times 5 ${stimFile}_5.1D 'BLOCK4(6,1)' \
        -stim_label 6 'GroupViewP1P2_MixInfRiskyNoConf' -stim_times 6 ${stimFile}_6.1D 'BLOCK4(6,1)' \
        -stim_label 7 'Single_ViewlotteriesRiskyConf' -stim_times 7 ${stimFile}_7.1D 'BLOCK4(6,1)' \
        -stim_label 8 'Single_ViewlotteriesRiskyNoConf' -stim_times 8 ${stimFile}_8.1D 'BLOCK4(6,1)' \
        -stim_label 9 'Keypress' -stim_times 9 ${stimFile}_9.1D 'BLOCK4(0.25,1)' \
        -stim_label 10 'Review' -stim_times 10 ${stimFile}_10.1D 'BLOCK4(2,1)'

3dREMLfit -overwrite -verb \
-mask data/cerebrumForAFNI.nii \
-matrix ${saveDir}/decon.xmat.1D \
-input ${imageFile} \
-gltsym 'SYM: +1*Group_ViewP1P2_RiskyInfConf -0.5*Single_ViewlotteriesRiskyConf -0.5*GroupViewP1P2_MixInfRiskyConf +0.5*Single_ViewlotteriesRiskyNoConf +0.5*GroupViewP1P2_MixInfRiskyNoConf -1*Group_ViewP1P2_RiskyInfNoConf' 'OCUrisky' \
-Rvar ${saveDir}/decon_REMLvar \
-Rglt ${saveDir}/OCUrisky \
-GOFORIT 9 \
-tout
