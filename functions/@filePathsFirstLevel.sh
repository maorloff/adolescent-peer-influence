#!/bin/bash
currSub=$1
currContrast=$2

subDir="fMRIdata/preprocessed/${currSub}/functional"
stimFileDir="data/stimFiles/${currContrast}SocialValue/${currSub}"

censorFile="${subDir}/censorFile.1D"
stimFile="${stimFileDir}/stimNum"
REMLscript="${subDir}/decon.REML_cmd"
saveDir="fMRIdata/firstLevelAFNI/${currContrast}SocialValue/${currSub}"
mkdir -p $saveDir
