#!/bin/bash

mkdir -p fMRIdata/secondLevel/safeSocialValueBetweenGroup/thresh005/
3dcalc -a fMRIdata/secondLevel/safeSocialValueBetweenGroup/spmT_0001.nii -expr 'step(abs(a)-2.7564)*a' -prefix fMRIdata/secondLevel/safeSocialValueBetweenGroup/thresh005/spmT_0001.nii
mkdir -p fMRIdata/secondLevel/riskySocial/thresh005/substExposed/
3dcalc -a fMRIdata/secondLevel/riskySocial/substExposed/spmT_0001.nii -expr 'step(abs(a)-2.7564)*a' -prefix fMRIdata/secondLevel/riskySocial/thresh005/substExposed/spmT_0003.nii
mkdir -p fMRIdata/secondLevel/riskySocial/thresh005/substNaive/
3dcalc -a fMRIdata/secondLevel/riskySocial/substNaive/spmT_0001.nii -expr 'step(abs(a)-2.7564)*a' -prefix fMRIdata/secondLevel/riskySocial/thresh005/substNaive/spmT_0003.nii
mkdir -p fMRIdata/secondLevel/safeSocial/thresh005/substExposed/
3dcalc -a fMRIdata/secondLevel/safeSocial/substExposed/spmT_0001.nii -expr 'step(abs(a)-2.7564)*a' -prefix fMRIdata/secondLevel/safeSocial/thresh005/substExposed/spmT_0003.nii
mkdir -p fMRIdata/secondLevel/safeSocial/thresh005/substNaive/
3dcalc -a fMRIdata/secondLevel/safeSocial/substNaive/spmT_0001.nii -expr 'step(abs(a)-2.7564)*a' -prefix fMRIdata/secondLevel/safeSocial/thresh005/substNaive/spmT_0003.nii
