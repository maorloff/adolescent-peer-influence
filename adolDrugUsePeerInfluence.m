clear; clc; close all hidden;

%% add function paths
addpath(genpath('functions'))

%% plot settings
allSubColor = [78 76 103] ./ 255;
substExposedColor = [1 .4 0];
substNaiveColor = [0 .5 0];
colorBlueDrugNo= [0 0 0.5];
colorBlueDrugYes=[0.6 0.6 1];
colorRedDrugNo=  [0.5 0 0];
colorRedDrugYes= [1 0.6 0.6];
maleColor = [.37 .4 .57];
femaleColor = [.68 .41 .55];
color15 = [.67 .4 .22];
color16 = [.67 .65 .22];
color17 = [.14 .42 .38];
lineWidth = 0.6;
smallMarkerSize = 25;
markerSize = 35;
largeMarkerSize = 75;

load('data/allVars')
load('data/ROIs')
if exist('data/reranROIs.mat')
    load('data/reranROIs')
    disp('OVERWRITING ROI VARIABLES WITH VALUES ACQUIRED AFTER RE-RUNNING FMRI ANALYSES')
end


%% load data
%parameter estimates
try %if you ran the model yourself, those parameters will load + overwrite existing OCU variables
    load('modelEstimates/OCU2');
    OCUsafe_trans = 1 ./ ( 1 + exp( -medianPosteriorBeta * medianPosteriorOCU) );
    OCUrisky_trans = 1 ./ ( 1 + exp( -medianPosteriorBeta * medianPosteriorOCU2) );
    disp('USING MODEL ESTIMATES FROM RE-ESTIMATED MODEL')
end

%subject group indicators
splitSubstExposed = anySubUse==1;
splitSubstNaive = anySubUse==0;

%% make figures!
makeFigure1
makeFigure2

%%SI figures
makeFigureS2
makeFigureS3
makeFigureS6
makeFigureS8
makeFigureS9
makeFigureS12
makeFigureS13
makeFigureS14
makeFigureS15
