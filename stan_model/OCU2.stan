data {
  int<lower=1> NumObservation;
  int<lower=1> NumSubject;
  int<lower=0,upper=1> behavCol_group[NumObservation]; //choice
  real probCol_group[NumObservation];                //probability
  real payoffCol_group[NumObservation,4];           //payoff
  int<lower=0,upper=1> conditionCol_group[NumObservation,2];  //conndition
}

parameters { 
  vector[4] mu_p; //mean of parameters
  vector<lower=0>[4] s_p; //variance of parameters
  vector[NumSubject] beta_raw;
  vector[NumSubject] rho_raw;
  vector[NumSubject] ocu_raw;
  vector[NumSubject] ocu2_raw;
}

transformed parameters {
  vector<lower=0>[NumSubject] beta; //inverse temp param
  vector<lower=0>[NumSubject] rho; //riskPref parameter 
  vector[NumSubject] ocu; //ocu safe parameter 
  vector[NumSubject] ocu2; //ocu risky parameter 

  beta = 50*Phi_approx(mu_p[1] + s_p[1] * beta_raw); //0 <= beta <= 50
  rho  = 2*Phi_approx(mu_p[2] + s_p[2] * rho_raw); //0 <= rho <= 2
  ocu  = mu_p[3] + s_p[3] * ocu_raw;
  ocu2 = mu_p[4] + s_p[4] * ocu2_raw;
}

model {
  int initIndex;
  
  //hyperparameters
  mu_p[1] ~ normal(0,10);
  mu_p[2] ~ normal(0,10);
  mu_p[3] ~ normal(0,10);
  mu_p[4] ~ normal(0,10);
  s_p  ~ cauchy(0,2.5);
  
  //individual parameters w/ "Matt trick"
  beta_raw ~ normal(0,1.0);
  rho_raw  ~ normal(0,1.0);
  ocu_raw  ~ normal(0,1.0);
  ocu2_raw  ~ normal(0,1.0);

  initIndex = 1;
  for (subjIndex in 1:NumSubject) {
    for (trialIndex in initIndex:(initIndex+95)) { //96 trials
      real Util_safe;
      real Util_risky;

      Util_safe  = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,1], rho[subjIndex]);
      Util_safe  = Util_safe + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,2], rho[subjIndex]);
      Util_safe  = Util_safe + conditionCol_group[trialIndex,1]* ocu[subjIndex];
      
      Util_risky = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,3], rho[subjIndex]);
      Util_risky = Util_risky + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,4], rho[subjIndex]);
      Util_risky = Util_risky + conditionCol_group[trialIndex,2]* ocu2[subjIndex];
      
      behavCol_group[trialIndex] ~ bernoulli_logit(beta[subjIndex] * (Util_safe - Util_risky)); //softmax
    }
    initIndex = initIndex+96;
  }
}

generated quantities { //for calculating log-likelihood
  real log_lik[NumSubject];
  int initIndex;

    initIndex = 1;
    for (subjIndex in 1:NumSubject) {
      log_lik[subjIndex] = 0;
      
      for (trialIndex in initIndex:(initIndex+95)) { //96 trials
        real Util_safe;
        real Util_risky;

        Util_safe  = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,1], rho[subjIndex]);
        Util_safe  = Util_safe + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,2], rho[subjIndex]);
        Util_safe  = Util_safe + conditionCol_group[trialIndex,1]* ocu[subjIndex];
        
        Util_risky = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,3], rho[subjIndex]);
        Util_risky = Util_risky + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,4], rho[subjIndex]);
        Util_risky = Util_risky + conditionCol_group[trialIndex,2]* ocu2[subjIndex];
        
        log_lik[subjIndex] = log_lik[subjIndex] + bernoulli_logit_log(behavCol_group[trialIndex], beta[subjIndex] * (Util_safe - Util_risky));
      }
      initIndex = initIndex+96;
  }
}
