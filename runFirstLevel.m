clear; clc; close all hidden;

%% add function path
addpath(genpath('functions'))

DMdir = 'data/DMs/';
preprocdatadir = 'fMRIdata/preprocessed/';

SPM12path = '/mnt/nfs/proj/pinfadol/spm12/'; %be sure to add your own path to SPM12 here

DMlist = dir(DMdir);
subsToRun = dir([preprocdatadir '/9*']);

for i = 3:length(DMlist)
    currDM = DMlist(i).name;
    if contains(currDM,'PPI')
        continue %PPIs need to be run after the safe + risky value social DMs
    end
    for j = 1:length(subsToRun)
        currSub = subsToRun(j).name;
        outputdir = ['fMRIdata/firstLevel/' currDM '/' currSub];
        contrastdir = 'data/contrastMATs/';
        if ~exist(outputdir,'dir')
            mkdir(outputdir)
        end
        
        firstLevel(preprocdatadir,currSub,[DMdir currDM '/'],[contrastdir currDM '/'],outputdir,SPM12path)
    end
end
