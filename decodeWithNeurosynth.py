import neurosynth as ns
from neurosynth.analysis import decode
from neurosynth.base.dataset import Dataset
from functions.utils import *

nsDir = 'data/neurosynthData/'

dataset = getDataset() #NOTE: you may need to get the 'database.txt' and 'features.txt' file from Neurosynth and add to 'data/neurosynthData/'

decoder = decode.Decoder(dataset)

imgBase = 'fMRIdata/secondLevel/safeSocialValueBetweenGroup/thresh005/'
decodeImage(decoder, imgBase, ['adolescents'], 1, 'data/reranOCUSafedrugDiffThresh005results.txt')
imgBase = 'fMRIdata/secondLevel/riskySocial/thresh005/'
decodeImage(decoder, imgBase, ['substNaive','substExposed'], 3, 'data/reransocialInfoRiskyThresh005results.txt')
imgBase = 'fMRIdata/secondLevel/safeSocial/thresh005/'
decodeImage(decoder, imgBase, ['substNaive','substExposed'], 3, 'data/reransocialInfoSafeThresh005results.txt')
