clear; clc; close all hidden;

%% add function path
addpath(genpath('functions'))

firstleveldir = 'fMRIdata/firstLevel/';

SPM12path = '/mnt/nfs/proj/pinfadol/spm12/'; %be sure to add your own path to SPM12 here

DMlist = dir(firstleveldir);

for i = 3:length(DMlist)
    currDMpath = [firstleveldir DMlist(i).name '/'];
    
    outputdir = ['fMRIdata/secondLevel/' DMlist(i).name '/'];
    if ~exist(outputdir)
        mkdir(outputdir)
    end
    
    contrastNum = 1;
    contrastName = DMlist(i).name;
    
    if contains(contrastName,'socialValue')
        continue %will run these below
    else
        secondLevel(currDMpath,outputdir,contrastNum,contrastName,SPM12path)
    end
    
end

currDMpath = [firstleveldir 'safeValueSocial/'];
outputdir = ['fMRIdata/secondLevel/safeSocialValue/'];
secondLevel(currDMpath,outputdir,1,'socialValue',SPM12path)
outputdir = ['fMRIdata/secondLevel/safeSocial/'];
secondLevel(currDMpath,outputdir,3,'social',SPM12path)

currDMpath = [firstleveldir 'riskyValueSocial/'];
outputdir = ['fMRIdata/secondLevel/riskySocialValue/'];
secondLevel(currDMpath,outputdir,1,'socialValue',SPM12path)
outputdir = ['fMRIdata/secondLevel/riskySocial/'];
secondLevel(currDMpath,outputdir,3,'social',SPM12path)

currDMpath = [firstleveldir 'safeValueSocial/'];
outputdir = ['fMRIdata/secondLevel/safeSocialValueBetweenGroup/'];
secondLevelTwoSample(currDMpath,outputdir,1,'safeSocialValue',SPM12path)
currDMpath = [firstleveldir 'riskyValueSocial/'];
outputdir = ['fMRIdata/secondLevel/riskySocialValueBetweenGroup/'];
secondLevelTwoSample(currDMpath,outputdir,1,'riskySocialValue',SPM12path)
