clear; clc; close all hidden;

contrastList = {'safe','risky'};

for currCon = 1:length(contrastList)
if exist(['fMRIdata/secondLevelAFNI/' contrastList{currCon} 'SocialValue'])
    voxels = importdata(['fMRIdata/secondLevelAFNI/' contrastList{currCon} 'SocialValue/MMvoxels.txt']);
    eval(['AFNIdataOCU' contrastList{currCon} ' = voxels;']);
    save('data/reranROIs',['AFNIdataOCU' contrastList{currCon}], '-append')
end

if exist(['fMRIdata/secondLevel/' contrastList{currCon} 'SocialValueBetweenGroup'])
    voxels = importdata(['fMRIdata/secondLevel/' contrastList{currCon} 'SocialValueBetweenGroup/voxels.txt']);
    eval(['SPMdataOCU' contrastList{currCon} ' = voxels;']);
    save('data/reranROIs',['SPMdataOCU' contrastList{currCon}],'-append')
end
end