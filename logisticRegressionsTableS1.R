library(R.matlab)
library(lmtest)
library(brms)
library(bayestestR)
rm(list=ls())

data = readMat('data/allVars.mat')

if ( file.exists('data/reranROIs.mat') ) {
  ROIs = readMat('data/reranROIs.mat')
} else {
  ROIs = readMat('data/ROIs.mat')
  warning('using previously calculated ROIs')
}


subUse = data$anySubUse

parentIncome = data$parentIncome
parentIncome<-apply(parentIncome, 2, function(x) ifelse(is.nan(x),NA,x)  )
parentIncome = factor(data$parentIncome,ordered = TRUE)

ARQantisocial = data$adolRiskQuestBehav.antisocial
ARQreckless = data$adolRiskQuestBehav.reckless
ARQthrill = data$adolRiskQuestBehav.thrill
age = data$age
gender = data$gender
barrattAttention = data$barratt2ndOrder.attention
barrattMotor = data$barratt2ndOrder.motor
barrattNonplanning = data$barratt2ndOrder.nonplan
ADHD = data$YSR.DSM.ADHD
ODD = data$YSR.DSM.oppDef
conduct = data$YSR.DSM.conduct

if ( file.exists('data/modelEstimates/OCU2.mat') ) {
  
} else {
  warning('using previously estimated parameters')
  OCUsafe = data$OCUsafe.trans
  OCUrisky = data$OCUrisky.trans
}

#remove all but first 31 [imaging] subjects
parentIncome = parentIncome[1:31]
gender = gender[1:31]
ARQantisocial = ARQantisocial[1:31]
ARQreckless = ARQreckless[1:31]
ARQthrill = ARQthrill[1:31]
age = age[1:31]
barrattAttention = barrattAttention[1:31]
barrattMotor = barrattMotor[1:31]
barrattNonplanning = barrattNonplanning[1:31]
ADHD = ADHD[1:31]
ODD = ODD[1:31]
ARQthrill = ARQthrill[1:31]
subUse = subUse[1:31]

ROIcenters = c('52.8.m32',
              'm50.16.m28',
              '50.m44.10',
              'm56.m58.20',
              'm2.m56.36',
              '18.m2.m14',
              'm20.m6.m20',
              '42.m46.m22')

#non-valuation social processing ROIs
for (i in 1:length(ROIcenters)) {
  print(' ')
  print(paste0('logistic regression model for ROI: ', ROIcenters[i]))
  loadSafeText = paste0('currSafe = ROIs$socialROIsafe.',ROIcenters[i])
  loadRiskyText = paste0('currRisky = ROIs$socialROIrisky.',ROIcenters[i])
  eval(parse(text = loadSafeText))
  eval(parse(text = loadRiskyText))
  
  socialmodel = glm(formula = subUse ~ barrattMotor + ODD + currSafe + currRisky, family=binomial(link='logit'))
  print(summary(socialmodel))
  print(exp(socialmodel$coefficients))
  print(exp(confint(socialmodel)))
}
