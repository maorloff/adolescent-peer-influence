clear; clc; close all hidden;

SPM12path = '/mnt/nfs/proj/pinfadol/spm12/'; %be sure to add your own path to SPM12 here
addpath(SPM12path)
addpath(genpath('functions'))

load('data/allVars')
anySubUse = anySubUse(1:31);
subList = dir('fMRIdata/preprocessed/9*');
FDcensor = 0.9;

%% make file of FDs
for i = 1:length(subList)
    FDs{i} = calculateFD(importdata(['fMRIdata/preprocessed/' subList(i).name '/functional/rp_asub-' subList(i).name '_task-pinf_bold.txt']));
end

%% replace high motion with NaNs for calculations
for i = 1:length(FDs)
    newFDs = FDs{i};
    newFDs(newFDs>FDcensor) = NaN;
    FDs{i} = newFDs;
end

%% get FD stats
meanFDs = cellfun(@nanmean,FDs);
excludedVols = cellfun(@(x) sum(isnan(x)),FDs);

%all subs
disp(['mean FD: ' num2str(mean(meanFDs)) ' ' char(177) ' ' num2str(std(meanFDs)) ', range: ' num2str(min(meanFDs)) '-' num2str(max(meanFDs))])
disp(['excluded vols: ' num2str(mean(excludedVols)) ' ' char(177) ' ' num2str(std(excludedVols)) ', range: ' num2str(min(excludedVols)) '-' num2str(max(excludedVols))])

%substance naive
disp(' ')
disp('substance-naive:')
currFDs = meanFDs(anySubUse==0);
currExcluded = excludedVols(anySubUse==0);
disp(['mean FD: ' num2str(mean(currFDs)) ' ' char(177) ' ' num2str(std(currFDs)) ', range: ' num2str(min(currFDs)) '-' num2str(max(currFDs))])
disp(['excluded vols: ' num2str(mean(currExcluded)) ' ' char(177) ' ' num2str(std(currExcluded)) ', range: ' num2str(min(currExcluded)) '-' num2str(max(currExcluded))])

%substance exposed
disp(' ')
disp('substance-exposed')
currFDs = meanFDs(anySubUse==1);
currExcluded = excludedVols(anySubUse==1);
disp(['mean FD: ' num2str(mean(currFDs)) ' ' char(177) ' ' num2str(std(currFDs)) ', range: ' num2str(min(currFDs)) '-' num2str(max(currFDs))])
disp(['excluded vols: ' num2str(mean(currExcluded)) ' ' char(177) ' ' num2str(std(currExcluded)) ', range: ' num2str(min(currExcluded)) '-' num2str(max(currExcluded))])

disp(' ')
[~,p] = ttest2(meanFDs(anySubUse==0),meanFDs(anySubUse==1));
disp(['group difference in mean FDs, P = ' num2str(p)])
[~,p] = ttest2(excludedVols(anySubUse==0),excludedVols(anySubUse==1));
disp(['group difference in number of excluded volumes, P  = ' num2str(p)])
