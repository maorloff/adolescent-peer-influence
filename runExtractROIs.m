clear; clc; close all hidden;

%% add function path
addpath(genpath('functions'))

if ~exist('data/reranROIs')
    save('data/reranROIs') %just to create empty file to add other variables to later
end

SPM12path = '/mnt/nfs/proj/pinfadol/spm12/'; %be sure to add your own path to SPM12 here
addpath(SPM12path)

secondleveldir = 'fMRIdata/secondLevel/';
ROIsToExtract = dir([secondleveldir '*']);

for i = 1:length(ROIsToExtract)
    switch ROIsToExtract(i).name
        case 'pmodCV'
            ROIcenters = [-4 30 32; ...
                34 18 8];
            saveNames = {'dACC_pmodCV'; ...
                'rInsula_pmodCV'};
            appendCoordinates = zeros(length(saveNames),1);
        case 'riskyPPI'
            ROIcenters = [2 56 20];
            saveNames = {'PPIrisky'};
            appendCoordinates = 0;
        case 'safePPI'
            ROIcenters = [2 56 20];
            saveNames = {'PPIsafe'};
            appendCoordinates = 0;
        case {'riskySocial','safeSocial'}
            ROIcenters = [2 56 20;
                52, 8, -32; ...
                -50, 16, -28; ...
                50, -44, 10; ...
                -56, -58, 20; ...
                -2, -56, 36; ...
                18, -2, -14; ...
                -20, -6, -20; ...
                42, -46, -22];
            if strcmp(ROIsToExtract(i).name,'riskySocial')
                saveNames = cell(length(ROIcenters),1);
                saveNames(:) = {'socialROIrisky'};
                saveNames(1) = {'riskyInfo'};
            else
                saveNames = cell(length(ROIcenters),1);
                saveNames(:) = {'socialROIsafe'};
                saveNames(1) = {'safeInfo'};
            end
            appendCoordinates = [0; ones(length(saveNames)-1,1)];
        case {'riskySocialValue','safeSocialValue'}
            ROIcenters = [-4 40 -8];
            if strcmp(ROIsToExtract(i).name,'riskySocialValue')
                saveNames = {'riskyValue'};
            else
                saveNames = {'safeValue'};
            end
            appendCoordinates = 0;
        otherwise
            continue %if not specified above, ROIs aren't used in analyses
    end
    
    contrastpath = [secondleveldir ROIsToExtract(i).name '/'];
    extractROIs(contrastpath,ROIcenters,saveNames,appendCoordinates)
end
